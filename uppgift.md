# Arbetsprov frontend

Frontendkatalogen och dess struktur är fri att ändra på efter eget val till exempel om man önskar köra Vue.js eller liknande.
Valfritt om man önskar köra vanilla JavaScript eller något ramverk.

## Instruktioner 
- Forka detta repo.
- Skapa en feature-branch.
- Implementera en tabell enligt data.json strukturen.
- Hämta json datan via ett REST-api.
- Implementera sortering på alla rubriker i tabellen både asc och desc.
- Implementera en sökfunktion till tabellen där enbart matchande data visas.
- När man klickar på en rad så ska ett anrop till /invoiceDetails apit göra för att hämta fakturadetaljer om den specifika raden.
  - Visa informationen i en dialogruta.
  - Skapa en knapp för med texten "Läst faktura".
    - När man klickar ska en POST gå iväg till /invoiceAccepted med id. 
- Alla rader som är lästa ska vara av färgen #ddffdd övriga ska vara vita.


### Formatering av tabellen
- Tabellen ska från start sorteras efter senaste datumet först.
- InvoiceType 
  - 0 ska skrivas ut som Kredit.
  - 1 ska skrivas ut som Debet.
- Amount ska alltid visas med två decimaler och avslutas med kr. 
- DueDate, visas enligt ÅÅÅÅ-MM-DD.
- Övrig data ska visas utan formatering.

### Vad vi kollar på
- Hur JavaScript, CSS och HTML är skrivet.
- Hur lång tidsåtgången är.

### Filstruktur
```
frontend/
  index.html
  readme.md
  package.json
  src/
    style.css
	  index.js
backend/
  data.json
  package.json
  readme.md
```
