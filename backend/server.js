const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')

const data = require('./data.json')

const app = express()
const porten = 8088

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})
app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())

app.get('/invoices', function(req, res) {
  res.json(data)
})

app.get('/invoiceDetails', function(req, res) {
  let returnData = data.invoices.filter(invoice => invoice.Id === parseInt(req.query.Id))
  res.json(returnData)
})

app.post('/invoiceAccepted', function(req, res) {
  let isValidInvoiceId = data.invoices.some(invoice => invoice.Id === req.body.Id)
  if (isValidInvoiceId) {
    data.invoices.map(invoice => {
      if (invoice.Id === req.body.Id) {
        invoice.accepted = true
      }
      return invoice
    })
    fs.writeFileSync('data.json', JSON.stringify(data))
    res.status(200).send(data.invoices.filter(invoice => invoice.Id === req.body.Id))
  } else {
    res.status(404).send('Det här var ju tråkigt!')
  }
})

console.log(`Server started on: http://localhost:${porten}`)

app.listen(porten)